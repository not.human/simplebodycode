from openpyxl import *
import re

class SimpleBodyCode():
    def __init__(self):
        self.namefile = "overlap_test.xlsx"
        self.wb = load_workbook(self.namefile)
        self.sheet = self.wb.active
        self.m_row = self.sheet.max_row 
        self.extractXL = []
        self.mfr = []
        self.eng = []

    def main(self):

        self.extractAppNotes(self.extractXL, 8)
        self.searchMfr()
        self.searchEngine()
        self.writeResult(self.mfr, 10)
        self.writeResult(self.eng, 9)


    def extractAppNotes(self, liste, c):
        for i in range(1, self.m_row + 1):
            cell_obj = self.sheet.cell(row = i+1, column = c)
            liste.append(str(cell_obj.value))

    def searchMfr(self):
        for cell in self.extractXL:
            myValue = re.search('[A-Z]+([0-9]*#|[0-9]+)', cell)
            if myValue is not None: 
                self.mfr.append(myValue[0])
            elif myValue is None:
                self.mfr.append("")

    def searchEngine(self):
        for cell in self.extractXL:
            myValue = re.search('[0-9]+[A-Z]+', cell)
            if myValue is not None: 
                self.eng.append(myValue[0])
            elif myValue is None:
                self.eng.append("")

    def writeResult(self,liste,c,):
        r = 2
        for v in liste:
            self.sheet.cell(row=r, column=c).value = v
            r += 1

        self.wb.save(self.namefile)


if __name__ == '__main__':
    simpleBodyCode=SimpleBodyCode()
    simpleBodyCode.main()